﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormCalc
{
    public partial class Form : System.Windows.Forms.Form
    {
        private bool _isFirstOperand;
        private bool _isOpeator;
        private bool _isSecondOperand;
        private string _firstOperand;
        private string _secondOperand;
        private enum Operation
        {
            Plus,
            Minus,
            Mult,
            Div,
        }
        private Operation _operation;

        private void AddOperand(string val)
        {
            if (_isOpeator)
            {
                if (!_isSecondOperand)
                {
                    _secondOperand = val;
                } else
                {
                    _secondOperand += val;
                }
                _isSecondOperand = true;
                textResult.Text = _secondOperand;
            } else
            {
                if (!_isFirstOperand)
                {
                    _firstOperand = val;
                } else
                {
                    _firstOperand += val;
                }
                _isFirstOperand = true;
                textResult.Text = _firstOperand;
            }  
        }

        private decimal Calculate()
        {
            decimal r = 0;
            decimal stOp = Decimal.Parse(_firstOperand);
            decimal ndOp = Decimal.Parse(_secondOperand);
            switch (_operation)
            {
                case Operation.Plus:
                    r = stOp + ndOp;
                    break;
                case Operation.Minus:
                    r = stOp - ndOp;
                    break;
                case Operation.Mult:
                    r = stOp * ndOp;
                    break;
                case Operation.Div:
                    r = stOp / ndOp;
                    break;
            }
            return r;
        }

        private void SetOperator(Operation op)
        {
            
            if (_isOpeator)
            {
                if (_isSecondOperand)
                {
                    
                    Result();
                    _operation = op;
                    _isOpeator = true;
                }
                   
            } else
            {
                _operation = op;
                _isOpeator = true;
            }
        }

        public Form()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textResult.Text = "0";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddOperand("1");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AddOperand("2");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AddOperand("3");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            AddOperand("4");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            AddOperand("5");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            AddOperand("6");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            AddOperand("7");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            AddOperand("8");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            AddOperand("9");
        }

        private void button0_Click(object sender, EventArgs e)
        {
            AddOperand("0");
        }

        private void button_comma_Click(object sender, EventArgs e)
        {
            AddOperand(",");
        }

        private void button_plus_Click(object sender, EventArgs e)
        {
            SetOperator(Operation.Plus);
        }

        private void button_minus_Click(object sender, EventArgs e)
        {
            SetOperator(Operation.Minus);
        }

        private void button_mult_Click(object sender, EventArgs e)
        {
            SetOperator(Operation.Mult);
        }

        private void button_div_Click(object sender, EventArgs e)
        {
            SetOperator(Operation.Div);
        }

        private void button_result_Click(object sender, EventArgs e)
        {
            Result();
        }

        private void Result()
        {
            if (_isFirstOperand && _isOpeator && _isSecondOperand)
            {
                var result = Calculate();
                textResult.Text = result.ToString();
                _isFirstOperand = true;
                _isOpeator = false;
                _isSecondOperand = false;
                _firstOperand = result.ToString();
                _secondOperand = "";

            }
        }

        private void button_clear_Click(object sender, EventArgs e)
        {
            _isFirstOperand = false;
            _isOpeator = false;
            _isSecondOperand = false;
            _firstOperand = "";
            _secondOperand = "";
            textResult.Text = "0";
        }
    }
}
